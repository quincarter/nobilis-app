// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyDxYEpKun_y2ynI1yeB2kWhW403t4tmmRs',
        authDomain: 'nobilis-app.firebaseapp.com',
        databaseURL: 'https://nobilis-app.firebaseio.com',
        projectId: 'nobilis-app',
        storageBucket: 'nobilis-app.appspot.com',
        messagingSenderId: '1073323283396',
        appId: '1:1073323283396:web:ca995701a41dd163'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
