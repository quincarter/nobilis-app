import {AfterViewInit, Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {NavigationEnd, Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements AfterViewInit {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
    ) {
        this.initializeApp();
    }

    ngAfterViewInit(): void {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                (<any> window).ga('set', 'page', event.urlAfterRedirects);
                (<any> window).ga('send', 'pageview');
            }
        });
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
