import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {GridListConfig} from '../models/grid';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root',
})
export class Tab1Resolver implements Resolve<GridListConfig[]> {
    constructor(private afs: AngularFirestore, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {
        return this.afs.collection('dashboard').valueChanges().pipe(
            mergeMap(gridList => {
                if (gridList) {
                    return of(gridList);
                } else { // id not found
                    this.router.navigate(['/']);
                    return EMPTY;
                }
            })
        );
    }
}
