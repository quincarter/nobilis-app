import {Component, OnInit} from '@angular/core';
import {PageTitle} from '../models/page-title';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {User} from '../core/user';
import {AuthService} from '../services/auth.service.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {DashboardService} from '../services/dashboard.service';
import {AlertController, ModalController} from '@ionic/angular';
import {CreateDashboardLinkPage} from '../pages/create-dashboard-link/create-dashboard-link.page';
import {GridListConfig} from '../models/grid';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
    public grid: Observable<any>;
    title: PageTitle;

    public user: User;
    public authenticated$: Observable<boolean>;
    private authSubscription: Subscription;

    constructor(
        public authService: AuthService,
        private angularFireAuth: AngularFireAuth,
        private route: ActivatedRoute,
        private afs: AngularFirestore,
        private dashboardService: DashboardService,
        private modalController: ModalController,
        private alertController: AlertController,
    ) {
    }

    ngOnInit(): void {
        this.authService.user$.subscribe(user => this.user = user);
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
        this.grid = this.afs.collection('dashboard').valueChanges();
        this.setTitle();
    }

    private setTitle() {
        this.title = {
            text: 'Dashboard',
            createLink: '/create-dashboard-link',
            image: 'nobilis_group-white.png',
            cssClass: 'app-logo',
        };
    }

    private getAuthSubscription() {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }

    async goToDetail(item: GridListConfig) {
        const modal = await this.modalController.create({
            component: CreateDashboardLinkPage,
            componentProps: {
                editableItem: {...item}
            },
        });
        await modal.present();
    }

    async delete(id: string, imageUrl: string) {
        const alert = await this.alertController.create({
            message: `Are you sure you want to delete this reference?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: cancel => {
                    },
                },
                {
                    text: 'Delete',
                    cssClass: 'danger',
                    handler: () => {
                        this.dashboardService.deleteDashboardItem(id, imageUrl).then(() => {
                            this.modalController.dismiss();
                        });
                    },
                },
            ],
        });

        await alert.present();
    }
}
