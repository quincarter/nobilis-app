import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab1Page} from './tab1.page';
import {Tab1RouterModule} from './tab1.router.module';
import {AdminButtonsModule} from '../components/admin-buttons/admin-buttons.module';
import {DashboardGridComponent} from '../components/dashboard-grid/dashboard-grid.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        Tab1RouterModule,
        AdminButtonsModule,
    ],
    entryComponents: [],
    declarations: [
        Tab1Page,
        DashboardGridComponent
    ]
})
export class Tab1PageModule {
}
