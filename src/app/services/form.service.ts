import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {BehaviorSubject, Observable} from 'rxjs';
import {GridListConfig} from '../models/grid';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private form: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public readonly form$: Observable<any> = this.form.asObservable();

  constructor(
      private afs: AngularFirestore
  ) { }

  public setFormByRouteName(path: string) {
    this.afs.collection('dashboard').valueChanges().subscribe((data: GridListConfig[]) => {
      if (data.length > 0) {
        for (const item of data) {
          if (item.path === path) {
            this.form.next(item);
          }
        }
      }
    });
  }
}
