import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireStorage} from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})
export class ImageService {

    private imageUrl: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    public imageUrl$: Observable<string> = this.imageUrl.asObservable();

    constructor(
        private storage: AngularFireStorage,
    ) {
    }

    setImagePath(path: string): void {
      this.imageUrl$ = this.storage.ref(path).getDownloadURL();
    }
}
