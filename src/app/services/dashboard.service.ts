import {Injectable} from '@angular/core';
import {ContactInterface} from '../models/contact.interface';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {GridListConfig} from '../models/grid';
import {AngularFireStorage} from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    constructor(
        public firestore: AngularFirestore,
        private storageFiles: AngularFireStorage,
    ) {
    }

    createItem(item: GridListConfig): Promise<void> {
        item.id = this.firestore.createId();
        return this.firestore.doc(`dashboard/${item.id}`).set(item);
    }

    getGridList(): AngularFirestoreCollection<ContactInterface> {
        return this.firestore.collection(`dashboard`);
    }

    updateItem(item: GridListConfig): Promise<void> {
        return this.firestore.doc(`dashboard/${item.id}`).update(item);
    }

    deleteDashboardItem(dashboardId: string, imageUrl?: string): Promise<void> {
        if (imageUrl) {
            this.storageFiles.storage.refFromURL(imageUrl).delete();
        }

        return this.firestore.doc(`dashboard/${dashboardId}`).delete();
    }
}
