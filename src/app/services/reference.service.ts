import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {GridListConfig} from '../models/grid';
import {ContactInterface} from '../models/contact.interface';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireStorage} from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})
export class ReferenceService {

    private reference: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly reference$: Observable<any> = this.reference.asObservable();

    constructor(
        public firestore: AngularFirestore,
        public storageFiles: AngularFireStorage,
    ) {
    }

    createItem(item: GridListConfig): Promise<void> {
        item.id = this.firestore.createId();
        return this.firestore.doc(`reference/${item.id}`).set(item);
    }

    setReferenceByPath(path): void {
        this.firestore.collection(`reference`).valueChanges().subscribe((data: GridListConfig[]) => {
            if (data.length > 0) {
                for (const item of data) {
                    if (item.path === path) {
                        this.reference.next(item);
                    }
                }
            }
        });
    }

    getGridList(): AngularFirestoreCollection<ContactInterface> {
        return this.firestore.collection(`reference`);
    }

    updateReference(item: GridListConfig): Promise<void> {
        return this.firestore.collection(`reference`).doc(item.id).update(item);
    }

    deleteReference(referenceId: string, documentUrl?: string, imageUrl?: string): Promise<void> {
        if (documentUrl) {
            this.storageFiles.storage.refFromURL(documentUrl).delete();
        }

        if (imageUrl) {
            this.storageFiles.storage.refFromURL(imageUrl).delete();
        }

        return this.firestore.doc(`reference/${referenceId}`).delete();
    }
}
