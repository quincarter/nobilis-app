import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from '../core/user';
import {Router} from '@angular/router';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import 'rxjs-compat/add/operator/switchMap';
import 'rxjs-compat/add/observable/of';
import {UserInterface} from '../models/user.interface';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private authenticatedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public readonly authenticated$: Observable<boolean> = this.authenticatedSubject.asObservable();

    user$: Observable<User>;
    private userData: Promise<void>;

    constructor(private angularFireAuth: AngularFireAuth,
                private afs: AngularFirestore,
                private router: Router) {
        //// Get auth data, then get firestore user document || null
        this.user$ = this.angularFireAuth.authState
            .switchMap(user => {
                if (user) {
                    return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
                } else {
                    return Observable.of(null);
                }
            });
    }

    private static async setUserData(userRef, user: UserInterface): Promise<void> {
        const data: User = {
            uid: user.uid,
            email: user.email,
            roles: {
                admin: false,
                editor: false,
                subscriber: true,
            }
        };
        return await userRef.set(data, {merge: true});
    }

    googleLogin() {
        const provider = new firebase.auth.GoogleAuthProvider();
        return this.oAuthLogin(provider);
    }

    private oAuthLogin(provider) {
        return this.angularFireAuth.auth.signInWithPopup(provider)
            .then((credential) => {
                this.updateUserData(credential.user);
            });
    }

    public getAuthUserId(): string {
        return this.angularFireAuth.auth.currentUser.uid;
    }

    signOut() {
        this.angularFireAuth.auth.signOut();
    }

    public updateUserData(user) {
        // Sets user data to firestore on login
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
        this.afs.collection('users').doc(user.uid).valueChanges().subscribe((userData: UserInterface) => {
            if (!userData) {
                this.userData = AuthService.setUserData(userRef, user);
            } else {
                this.userData = userRef.set(userData, {merge: true});
            }
        });
    }

    public setAuthenticated(auth: boolean) {
        this.authenticatedSubject.next(auth);
    }

    ///// Role-based Authorization //////

    canRead(user: User): boolean {
        const allowed = ['admin', 'editor', 'subscriber'];
        return this.checkAuthorization(user, allowed);
    }

    canEdit(user: User): boolean {
        const allowed = ['admin', 'editor'];
        return this.checkAuthorization(user, allowed);
    }

    canDelete(user: User): boolean {
        const allowed = ['admin'];
        return this.checkAuthorization(user, allowed);
    }


// determines if user has matching role
    private checkAuthorization(user: User, allowedRoles: string[]): boolean {
        if (!user) {
            return false;
        }
        for (const role of allowedRoles) {
            if (user.roles[role]) {
                return true;
            }
        }
        return false;
    }
}
