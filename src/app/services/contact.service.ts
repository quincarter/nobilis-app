import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {ContactInterface} from '../models/contact.interface';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContactService {

    private contact: BehaviorSubject<ContactInterface> = new BehaviorSubject<ContactInterface>(null);
    public readonly contact$: Observable<ContactInterface> = this.contact.asObservable();

    constructor(
        public firestore: AngularFirestore,
    ) {
    }

    getContactValue(): any {
        return this.contact.getValue();
    }

    createContact(contact: ContactInterface): Promise<void> {
        contact.id = this.firestore.createId();
        return this.firestore.doc(`contactList/${contact.id}`).set(contact);
    }

    getContactList(): AngularFirestoreCollection<ContactInterface> {
        return this.firestore.collection(`contactList`);
    }

    getContactDetail(contactId: string): AngularFirestoreDocument<ContactInterface> {
        this.firestore.collection('contactList').doc(contactId).valueChanges().subscribe((data: ContactInterface) => {
            this.contact.next(data);
        });
        return this.firestore.collection('contactList').doc(contactId);
    }

    updateContact(contact: ContactInterface): Promise<void> {
        return this.firestore.doc(`contactList/${contact.id}`).update(contact);
    }

    deleteContact(contactId: string): Promise<void> {
        return this.firestore.doc(`contactList/${contactId}`).delete();
    }
}
