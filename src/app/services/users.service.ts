import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserInterface} from '../models/user.interface';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    private users: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public readonly users$: Observable<any> = this.users.asObservable();

    constructor(
        public firestore: AngularFirestore,
    ) {
    }

    public getUsersList(): AngularFirestoreCollection<UserInterface> {
        return this.firestore.collection(`users`);
    }

    public getUser(id: string): AngularFirestoreDocument<UserInterface> {
        this.users.next(this.firestore.collection('users').doc(id).valueChanges());
        return this.firestore.collection('users').doc(id);
    }

    public updateUser(user: UserInterface): Promise<void> {
        return this.firestore.collection(`users`).doc(user.uid).update(user);
    }
}
