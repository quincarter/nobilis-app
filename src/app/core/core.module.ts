import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CanReadGuard} from './can-read.guard';
import {AdminGuard} from './admin.guard';
import {AuthService} from '../services/auth.service.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        AuthService,
        AdminGuard,
        CanReadGuard
    ]
})
export class CoreModule {
}
