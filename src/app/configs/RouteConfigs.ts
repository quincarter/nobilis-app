import {Routes} from '@angular/router';
import {FormPage} from '../pages/form/form.page';

export const formRoutes: Routes = [{
    path: '**',
    component: FormPage,
    pathMatch: 'full'
}];
