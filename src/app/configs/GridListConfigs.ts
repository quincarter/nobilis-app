import {GridListConfig} from '../models/grid';

export const gridListConfigs: GridListConfig[] = [{
    path: 'cat_travel',
    image: 'cat_travel.png',
    label: 'CAT Travel',
    alt: 'CAT Travel',
    link: 'https://nobilisgroup.formstack.com/forms/travelrequest',
}, {
    path: 'sales_travel',
    image: 'sales.png',
    label: 'Sales Travel',
    alt: 'Sales Travel',
    link: 'https://nobilisgroup.formstack.com/forms/sales_travel',
}, /*{
    path: 'marketing_request',
    image: 'marketing_request.png',
    label: 'Marketing Request',
    alt: 'Marketing Request',
    link: 'https://nobilisgroup.formstack.com/forms/marketing_request',
}, */{
    path: 'csi_survey',
    image: 'csi_survey.png',
    label: 'CSI Survey',
    alt: 'CSI Survey',
    link: 'https://nobilisgroup.formstack.com/forms/customer_survey',
}, {
    path: 'shirt_order',
    image: 'shirt_order.png',
    label: 'Shirt Order',
    alt: 'Shirt Order',
    link: 'https://nobilisgroup.formstack.com/forms/pdrlinx_apparel',
}, {
    path: 'field_supplies',
    image: 'field_supplies.png',
    label: 'Field Supplies',
    alt: 'Field Supplies',
    link: 'https://nobilisgroup.formstack.com/forms/supplies_request',
}, {
    path: 'vehicle_requests',
    image: 'vehicle_maintenance.png',
    label: 'Vehicle Requests',
    alt: 'Vehicle Requests',
    link: 'https://nobilisgroup.formstack.com/forms/maintenance',
}];
