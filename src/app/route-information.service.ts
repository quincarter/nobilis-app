import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { GridListConfig } from './models/grid';

@Injectable({
  providedIn: 'root'
})
export class RouteInformationService {

  private routeInfo: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public readonly routeInfo$: Observable<any> = this.routeInfo.asObservable();

  constructor() { }

  public setRouteInfo(info: GridListConfig[]): void {
    this.routeInfo.next(info);
  }
}
