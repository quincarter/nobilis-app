import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoadingController, ModalController} from '@ionic/angular';
import {ContactService} from '../../services/contact.service';

@Component({
    selector: 'app-create-contact',
    templateUrl: './create-contact.page.html',
    styleUrls: ['./create-contact.page.scss'],
})
export class CreateContactPage implements OnInit {
    public createContactForm: FormGroup;
    public phone: string;

    constructor(
        public loadingCtrl: LoadingController,
        public contactService: ContactService,
        public router: Router,
        private modalController: ModalController,
        formBuilder: FormBuilder
    ) {
        this.createContactForm = formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            title: ['', Validators.required],
            phone: ['', Validators.required],
            email: ['', Validators.required],
        });
    }

    ngOnInit() {
        this.phone = this.createContactForm.value.phone;
    }

    async createContact() {
        const loading = await this.loadingCtrl.create({});

        const contact = {
            id: null,
            firstName: this.createContactForm.value.firstName,
            lastName: this.createContactForm.value.lastName,
            title: this.createContactForm.value.title,
            phone: this.createContactForm.value.phone,
            email: this.createContactForm.value.email,
        };
        this.contactService
            .createContact(contact)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.closeModal();
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    closeModal() {
        this.modalController.dismiss();
    }
}
