import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CreateContactPage} from './create-contact.page';
import {PhonePipeModule} from '../../pipes/phone/phone-pipe.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        PhonePipeModule,
    ],
    declarations: [
        CreateContactPage,
    ]
})
export class CreateContactPageModule {
}
