import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {LoginPage} from './login.page';
import {NgxErrorsModule} from '@ultimate/ngxerrors';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {FirebaseUIModule} from 'firebaseui-angular';
import {AuthService} from '../../services/auth.service.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        NgxErrorsModule,
        AngularFireAuthModule,
        FirebaseUIModule
    ],
    declarations: [LoginPage],
    exports: [
        LoginPage
    ],
    providers: [
        AuthService
    ]
})
export class LoginPageModule {
}
