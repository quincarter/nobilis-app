import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    constructor(
        private modalController: ModalController,
        private angularFireAuth: AngularFireAuth,
        private authService: AuthService,
    ) {
    }

    ngOnInit(): void {
        this.angularFireAuth.authState.subscribe(data => {
            this.firebaseAuthChangeListener(data);
        });
    }

    closeModal() {
        this.modalController.dismiss();
    }

    private firebaseAuthChangeListener(user) {
        // if needed, do a redirect in here
        if (user) {
            this.authService.updateUserData(user);
            this.modalController.dismiss();
        }
    }
}
