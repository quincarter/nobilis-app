import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ContactDetailPage} from './contact-detail.page';
import {PhonePipeModule} from '../../pipes/phone/phone-pipe.module';
import {PopoverModule} from 'ngx-smart-popover';
import {ClipboardModule} from 'ngx-clipboard';

@NgModule({
    imports: [
        CommonModule,
        PopoverModule,
        ClipboardModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        PhonePipeModule,
    ],
    declarations: [
        ContactDetailPage,
    ],
})
export class ContactDetailPageModule {
}
