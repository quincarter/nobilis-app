import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {ContactInterface} from '../../models/contact.interface';
import {ContactService} from '../../services/contact.service';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../core/user';

@Component({
    selector: 'app-contact-detail',
    templateUrl: './contact-detail.page.html',
    styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit, OnDestroy {
    public user: User;
    public contact: ContactInterface;
    public contactId: string;
    public updateContactForm: FormGroup;

    public readOnlyMode = true;
    public authenticated$: Observable<boolean>;
    private authSubscription: Subscription;

    @ViewChild('updateForm') updateFormElement;

    constructor(
        private contactService: ContactService,
        private router: Router,
        private route: ActivatedRoute,
        private alertController: AlertController,
        private angularFireAuth: AngularFireAuth,
        public authService: AuthService,
        private formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        private modalController: ModalController,
    ) {
    }

    ngOnInit(): void {
        this.authService.user$.subscribe(user => this.user = user);
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
        this.contactId = this.contact.id;
    }

    ngOnDestroy(): void {
        this.authSubscription.unsubscribe();
    }

    async deleteContact() {
        const alert = await this.alertController.create({
            message: `Are you sure you want to delete this contact?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: cancel => {
                    },
                },
                {
                    text: 'Delete',
                    cssClass: 'danger',
                    handler: () => {
                        this.contactService.deleteContact(this.contactId).then(() => {
                            this.modalController.dismiss();
                        });
                    },
                },
            ],
        });

        await alert.present();
    }

    async updateContact() {
        const loading = await this.loadingCtrl.create({});

        const contact = {
            id: this.contactId,
            firstName: this.updateContactForm.value.firstName,
            lastName: this.updateContactForm.value.lastName,
            title: this.updateContactForm.value.title,
            phone: this.updateContactForm.value.phone,
            email: this.updateContactForm.value.email,
        };

        // resetting the view part on the card
        this.contact = {...contact};

        this.contactService
            .updateContact(contact)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.readOnlyMode = true;
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    toggleEditMode(toggle: boolean): void {
        this.readOnlyMode = toggle;
        if (!toggle) {
            this.updateContactForm = this.formBuilder.group({
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                title: ['', Validators.required],
                phone: ['', Validators.required],
                email: ['', Validators.compose([Validators.required, Validators.email])],
            });

            this.updateContactForm.patchValue({
                firstName: this.contact.firstName,
                lastName: this.contact.lastName,
                title: this.contact.title,
                phone: this.contact.phone,
                email: this.contact.email,
            });
        }
    }


    submitForm() {
        this.updateContact();
        this.toggleEditMode(true);
    }

    closeModal() {
        this.modalController.dismiss();
    }

    private getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }
}
