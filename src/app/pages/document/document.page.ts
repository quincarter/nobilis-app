import {Component, OnDestroy, OnInit} from '@angular/core';
import {ReferenceService} from '../../services/reference.service';
import {ActivatedRoute} from '@angular/router';
import {GridListConfig} from '../../models/grid';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-document',
    templateUrl: './document.page.html',
    styleUrls: ['./document.page.scss'],
})
export class DocumentPage implements OnInit, OnDestroy {
    public reference: GridListConfig;
    private referenceSubscription: Subscription;
    private currentRoute: string;

    constructor(
        private referenceService: ReferenceService,
        private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.getReferenceSubscription();
        this.route.url.subscribe(data => {
            this.currentRoute = data[0].path;
        });
        this.getReferenceData();
    }

    ngOnDestroy(): void {
        this.referenceSubscription.unsubscribe();
    }

    private getReferenceSubscription() {
        this.referenceSubscription = this.referenceService.reference$.subscribe((data) => {
            this.reference = data;
        });
    }

    private getReferenceData() {
        this.referenceService.setReferenceByPath(this.currentRoute);
    }
}
