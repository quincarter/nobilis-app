import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {DocumentPage} from './document.page';
import {NgxDocViewerModule} from 'ngx-doc-viewer';

const routes: Routes = [
    {
        path: '**',
        component: DocumentPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        NgxDocViewerModule,
    ],
    declarations: [DocumentPage]
})
export class DocumentPageModule {
}
