import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {FormPage} from './form.page';
import {formRoutes} from '../../configs/RouteConfigs';
import {SafePipe} from '../../pipes/safe.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(formRoutes),
    ],
    declarations: [FormPage, SafePipe]
})
export class FormPageModule {
}
