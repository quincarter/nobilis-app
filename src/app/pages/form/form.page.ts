import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GridListConfig} from '../../models/grid';
import {gridListConfigs} from '../../configs/GridListConfigs';
import {FormService} from '../../services/form.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-form',
    templateUrl: './form.page.html',
    styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit, OnDestroy {

    public currentRoute: string;
    public gridListConfgs: GridListConfig[];
    private formSubscription: Subscription;
    public form: GridListConfig;

    constructor(
        private route: ActivatedRoute,
        private formService: FormService,
    ) {
    }

    ngOnInit(): void {
        this.getFormSubscription();
        this.gridListConfgs = gridListConfigs;
        this.route.url.subscribe(data => {
            this.currentRoute = data[0].path;
        });
        this.getFormData();
    }

    ngOnDestroy(): void {
        this.formSubscription.unsubscribe();
    }

    private getFormData(): void {
        this.formService.setFormByRouteName(this.currentRoute);
    }

    private getFormSubscription(): void {
        this.formSubscription = this.formService.form$.subscribe((data) => {
            this.form = data;
        });
    }
}
