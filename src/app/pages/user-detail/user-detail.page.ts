import {Component} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {UserInterface} from '../../models/user.interface';
import {AuthService} from '../../services/auth.service.service';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.page.html',
    styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage {

    public user: UserInterface;

    constructor(
        public authService: AuthService,
        private modalController: ModalController,
    ) {
    }

    closeModal() {
        this.modalController.dismiss();
    }
}
