import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {UserDetailPage} from './user-detail.page';
import {UserDetailListItemModule} from '../../components/user-detail-list-item/user-detail-list-item.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        UserDetailListItemModule
    ],
    exports: [
        UserDetailPage
    ],
    declarations: [UserDetailPage]
})
export class UserDetailPageModule {
}
