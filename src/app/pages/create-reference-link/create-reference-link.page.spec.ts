import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReferenceLinkPage } from './create-reference-link.page';

describe('CreateReferenceLinkPage', () => {
  let component: CreateReferenceLinkPage;
  let fixture: ComponentFixture<CreateReferenceLinkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReferenceLinkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReferenceLinkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
