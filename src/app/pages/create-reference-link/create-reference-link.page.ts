import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GridListConfig} from '../../models/grid';
import {Observable} from 'rxjs';
import {AngularFireStorage} from '@angular/fire/storage';
import {ReferenceService} from '../../services/reference.service';

@Component({
    selector: 'app-create-reference-link',
    templateUrl: './create-reference-link.page.html',
    styleUrls: ['./create-reference-link.page.scss'],
})
export class CreateReferenceLinkPage implements OnInit {
    public createReferenceLinkForm: FormGroup;
    public editReferenceLinkForm: FormGroup;
    public editableItem: GridListConfig;
    public downloadUrl$: string;
    public pdfDownloadUrl$: string;

    private imageUrl: string;
    private documentUrl: string;

    constructor(
        public loadingCtrl: LoadingController,
        public authService: AuthService,
        private storage: AngularFireStorage,
        private angularFireAuth: AngularFireAuth,
        private referenceService: ReferenceService,
        private modalController: ModalController,
        formBuilder: FormBuilder
    ) {
        this.createReferenceLinkForm = formBuilder.group({
            itemName: ['', Validators.required],
            image: [''],
            document: [''],
        });

        this.editReferenceLinkForm = formBuilder.group({
            itemName: ['', Validators.required],
            image: [''],
            document: [''],
        });

        this.markFormGroupTouched(this.createReferenceLinkForm);
    }

    ngOnInit(): void {
        if (this.editableItem) {
            this.setImage(this.editableItem.image);
            this.setPDF(this.editableItem.document);
            this.editReferenceLinkForm.patchValue({
                itemName: this.editableItem.label,
                image: this.editableItem.image,
                document: this.editableItem.document,
            });

            this.markFormGroupTouched(this.editReferenceLinkForm);
            this.editReferenceLinkForm.markAsDirty();
        }
    }

    closeModal() {
        this.modalController.dismiss();
    }

    async createReferenceLinkItem() {
        const loading = await this.loadingCtrl.create({});

        const referenceLinkItem: GridListConfig = {
            id: null,
            path: this.getPath(this.createReferenceLinkForm.value.itemName),
            label: this.getTitleCase(this.createReferenceLinkForm.value.itemName),
            alt: this.getTitleCase(this.createReferenceLinkForm.value.itemName),
            link: null,
            image: this.imageUrl,
            document: this.documentUrl,
        };
        this.referenceService
            .createItem(referenceLinkItem)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.closeModal();
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    async editReferenceLinkItem() {
        const loading = await this.loadingCtrl.create({});

        const referenceLinkItem: GridListConfig = {
            id: this.editableItem.id,
            path: this.getPath(this.editReferenceLinkForm.value.itemName),
            label: this.getTitleCase(this.editReferenceLinkForm.value.itemName),
            alt: this.getTitleCase(this.editReferenceLinkForm.value.itemName),
            link: null,
            image: this.imageUrl,
            document: this.documentUrl,
        };
        this.referenceService
            .updateReference(referenceLinkItem)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.closeModal();
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    private getPath(itemName: string): string {
        return itemName.trim().toLowerCase().split(' ').join('_');
    }

    private getTitleCase(itemName: string) {
        return itemName.trim().toLowerCase()
            .split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ');
    }

    setImage($event: Observable<string> | string | null) {
        if (typeof $event === 'string') {
            this.downloadUrl$ = $event;
        } else if (typeof $event === 'object' && $event !== null) {
            $event.subscribe(data => {
                this.downloadUrl$ = data;
            });
        } else if ($event === null) {
            this.downloadUrl$ = null;
        }
    }

    getImageValue(url: string) {
        this.imageUrl = url;
        return url;
    }

    private markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }

    setPDF($event: Observable<string> | string | null) {
        if (typeof $event === 'string') {
            this.pdfDownloadUrl$ = $event;
        } else if (typeof $event === 'object' && $event !== null) {
            $event.subscribe(data => {
                this.pdfDownloadUrl$ = data;
            });
        } else if (!$event) {
            this.pdfDownloadUrl$ = null;
        }
    }

    getPdfDocumentValue(pdfUrl: string) {
        this.documentUrl = pdfUrl;
        return pdfUrl;
    }

    async updateReferenceItem(item: GridListConfig, type: string) {
        if (type === 'pdf') {
            item.document = null;
            await this.referenceService.updateReference(item);
        } else if (type === 'image') {
            item.image = null;
            await this.referenceService.updateReference(item);
        }
    }
}
