import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {CreateReferenceLinkPage} from './create-reference-link.page';
import {FileUploadModule} from '../../components/file-upload/file-upload.module';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      IonicModule,
      FileUploadModule,
      ReactiveFormsModule
    ],
    declarations: [CreateReferenceLinkPage]
})
export class CreateReferenceLinkPageModule {
}
