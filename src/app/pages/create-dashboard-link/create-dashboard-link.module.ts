import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {CreateDashboardLinkPage} from './create-dashboard-link.page';
import {AdminGuard} from '../../core/admin.guard';
import {FileUploadModule} from '../../components/file-upload/file-upload.module';
import {FileSizePipe} from '../../file-size.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        FileUploadModule,
        ReactiveFormsModule
    ],
    providers: [
        FileSizePipe
    ],
    declarations: [CreateDashboardLinkPage]
})
export class CreateDashboardLinkPageModule {
}
