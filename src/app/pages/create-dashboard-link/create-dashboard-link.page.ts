import {Component, OnInit} from '@angular/core';
import {LoadingController, ModalController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DashboardService} from '../../services/dashboard.service';
import {GridListConfig} from '../../models/grid';
import {Observable, Subscription} from 'rxjs';
import {AngularFireStorage} from '@angular/fire/storage';
import {ImageService} from '../../services/image.service';

@Component({
    selector: 'app-create-dashboard-link',
    templateUrl: './create-dashboard-link.page.html',
    styleUrls: ['./create-dashboard-link.page.scss'],
})
export class CreateDashboardLinkPage implements OnInit {
    createDashboardLinkForm: FormGroup;
    editDashboardLinkForm: FormGroup;

    editableItem: GridListConfig;

    public downloadUrl$: string;

    private downloadUrlSubscription: Subscription;
    private imageUrl: string;
    public id: string;
    public path: string;

    constructor(
        public loadingCtrl: LoadingController,
        public authService: AuthService,
        public imageService: ImageService,
        private storage: AngularFireStorage,
        private dashboardService: DashboardService,
        private angularFireAuth: AngularFireAuth,
        private modalController: ModalController,
        formBuilder: FormBuilder
    ) {
        this.createDashboardLinkForm = formBuilder.group({
            itemName: ['', Validators.required],
            formstackLink: ['', Validators.required],
            image: [''],
        });

        this.editDashboardLinkForm = formBuilder.group({
            itemName: ['', Validators.required],
            formstackLink: ['', Validators.required],
            image: [''],
        });

        this.markFormGroupTouched(this.createDashboardLinkForm);
    }

    ngOnInit(): void {
        if (this.editableItem) {
            this.setImage(this.editableItem.image);
            this.editDashboardLinkForm.patchValue({
                formstackLink: this.editableItem.link,
                itemName: this.editableItem.label,
                image: this.editableItem.image,
            });
            this.markFormGroupTouched(this.editDashboardLinkForm);
            this.editDashboardLinkForm.markAsDirty();
        }
    }

    closeModal() {
        this.modalController.dismiss();
    }

    async createDashboardItem() {
        const loading = await this.loadingCtrl.create({});

        const dashboardItem: GridListConfig = {
            id: null,
            path: this.getPath(this.createDashboardLinkForm.value.itemName),
            label: this.getTitleCase(this.createDashboardLinkForm.value.itemName),
            alt: this.getTitleCase(this.createDashboardLinkForm.value.itemName),
            link: this.createDashboardLinkForm.value.formstackLink,
            image: this.imageUrl,
        };
        this.dashboardService
            .createItem(dashboardItem)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.closeModal();
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    async editDashboardItem() {
        const loading = await this.loadingCtrl.create({});

        const dashboardItem: GridListConfig = {
            id: this.editableItem.id,
            path: this.getPath(this.editDashboardLinkForm.value.itemName),
            label: this.getTitleCase(this.editDashboardLinkForm.value.itemName),
            alt: this.getTitleCase(this.editDashboardLinkForm.value.itemName),
            link: this.editDashboardLinkForm.value.formstackLink,
            image: this.imageUrl,
        };
        this.dashboardService
            .updateItem(dashboardItem)
            .then(
                () => {
                    loading.dismiss().then(() => {
                        this.closeModal();
                    });
                },
                error => {
                    console.error(error);
                }
            );

        return await loading.present();
    }

    private getPath(itemName: string): string {
        return itemName.trim().toLowerCase().split(' ').join('_');
    }

    private getTitleCase(itemName: string) {
        return itemName.trim().toLowerCase()
            .split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ');
    }

    setImage($event: Observable<string> | string) {
        if (typeof $event === 'object' && $event !== null) {
            $event.subscribe(data => {
                this.downloadUrl$ = data;
            });
        } else if (typeof $event === 'string') {
            this.downloadUrl$ = $event;
        } else if (!$event) {
            this.downloadUrl$ = null;
        }
    }

    getImageValue(url: string) {
        this.imageUrl = url;
        return url;
    }

    private markFormGroupTouched(formGroup: FormGroup) {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();

            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }

    async updateDashboardItemImage(item: GridListConfig) {
        item.image = null;
        await this.dashboardService.updateItem(item);
    }
}
