import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDashboardLinkPage } from './create-dashboard-link.page';

describe('CreateDashboardLinkPage', () => {
  let component: CreateDashboardLinkPage;
  let fixture: ComponentFixture<CreateDashboardLinkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDashboardLinkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDashboardLinkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
