import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service.service';
import {User} from '../core/user';
import {Observable, Subscription} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

    public user: User;
    public authenticated$: Observable<boolean>;
    private authSubscription: Subscription;

    constructor(
        public authService: AuthService,
        private angularFireAuth: AngularFireAuth,
    ) {
    }

    ngOnInit(): void {
        this.authService.user$.subscribe(user => this.user = user);
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
    }

    private getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }
}
