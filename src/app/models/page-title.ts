export declare type PageTitles = 'Reference' | 'Contact' | 'Dashboard' | 'Users';

export interface PageTitle {
    text: PageTitles;
    createLink?: string;
    image?: string;
    cssClass?: string;
}
