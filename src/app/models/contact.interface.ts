export interface ContactInterface {
    id: string;
    firstName: string;
    lastName: string;
    title: string;
    phone: number;
    email: string;
}
