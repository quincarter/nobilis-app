export declare type RoleTypes = 'Admin' | 'Editor' | 'Subscriber';

export interface ChipObject {
    cssClass?: string;
    name: string;
    icon?: string;
}

export interface IRoles {
    admin?: boolean;
    editor?: boolean;
    subscriber: boolean;
}

export interface UserInterface {
    uid: string;
    email: string;
    roles: IRoles;
}
