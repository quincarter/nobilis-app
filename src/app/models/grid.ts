export class Grid {
    constructor() {}
}

export interface GridListConfig {
    id?: string;
    path: string;
    image: string;
    label: string;
    alt: string;
    link?: string; // required for dashboard, not for reference page
    document?: string; // only for reference pages
}
