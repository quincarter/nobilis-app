import {Component, Input, OnInit} from '@angular/core';
import {UserInterface} from '../../models/user.interface';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {

  @Input() users: UserInterface[];

  constructor() { }

  ngOnInit() {}

}
