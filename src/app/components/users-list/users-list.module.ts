import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersListComponent} from './users-list.component';
import {IonicModule} from '@ionic/angular';
import {UserListItemModule} from '../user-list-item/user-list-item.module';

@NgModule({
    declarations: [
        UsersListComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        UserListItemModule
    ],
    exports: [
        UsersListComponent
    ]
})
export class UsersListModule {
}
