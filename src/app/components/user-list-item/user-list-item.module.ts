import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserListItemComponent} from './user-list-item.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
  declarations: [
    UserListItemComponent
  ],
  exports: [
    UserListItemComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class UserListItemModule {
}
