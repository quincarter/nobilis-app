import {Component, Input, OnInit} from '@angular/core';
import {ChipObject, RoleTypes, UserInterface} from '../../models/user.interface';
import {ContactDetailPage} from '../../pages/contact-detail/contact-detail.page';
import {ModalController} from '@ionic/angular';
import {UserDetailPage} from '../../pages/user-detail/user-detail.page';
import {AuthService} from '../../services/auth.service.service';

@Component({
    selector: 'app-user-list-item',
    templateUrl: './user-list-item.component.html',
    styleUrls: ['./user-list-item.component.scss'],
})
export class UserListItemComponent implements OnInit {

    @Input() user: UserInterface;
    highestAccess: RoleTypes;
    chipObject: ChipObject;

    constructor(
        public authService: AuthService,
        private modalController: ModalController,
    ) {
    }

    ngOnInit() {
        this.getHighestAccess();
    }

    private getHighestAccess(): void {
        this.chipObject = {
            name: null,
            cssClass: null,
            icon: null
        };
        if (this.user.roles.admin === true) {
            this.highestAccess = 'Admin';
            this.chipObject.cssClass = 'adminChip';
            this.chipObject.icon = 'key';
        } else {
            if (this.user.roles.editor === true) {
                this.highestAccess = 'Editor';
                this.chipObject.cssClass = 'editorChip';
                this.chipObject.icon = 'create';
            } else {
                this.highestAccess = 'Subscriber';
                this.chipObject.cssClass = '';
            }
        }
        this.chipObject.name = this.highestAccess;
    }

    async goToDetail(userData): Promise<void> {
        const modal = await this.modalController.create({
            component: UserDetailPage,
            componentProps: {user: {...userData}},
        });
        await modal.present();
    }
}
