import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FileUploadComponent} from './file-upload.component';
import {FileSizePipe} from '../../file-size.pipe';
import {DropZoneDirective} from '../../drop-zone.directive';
import {IonicModule} from '@ionic/angular';

@NgModule({
    declarations: [
        DropZoneDirective,
        FileUploadComponent,
        FileSizePipe
    ],
    exports: [
        FileUploadComponent
    ],
    imports: [
        CommonModule,
        IonicModule
    ]
})
export class FileUploadModule {
}
