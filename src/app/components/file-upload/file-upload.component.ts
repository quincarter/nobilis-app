import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {Observable} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';
import {finalize, tap} from 'rxjs/operators';
import {ImageService} from '../../services/image.service';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {

    @Input() type: string;
    @Input() downloadURLForEdit: string;
    // Main task
    task: AngularFireUploadTask;

    // Progress monitoring
    percentage: Observable<number>;

    snapshot: Observable<any>;

    // Download URL
    downloadURL: Observable<string>;

    @Output() imageUrl: EventEmitter<any> = new EventEmitter();
    @Output() pdfUrl: EventEmitter<any> = new EventEmitter();
    @Output() fileDeleted: EventEmitter<any> = new EventEmitter();

    // State for dropzone CSS toggling
    isHovering: boolean;
    private resourceName: string;

    constructor(
        private storage: AngularFireStorage,
        private db: AngularFirestore,
        private imageService: ImageService,
        private alertController: AlertController,
    ) {
    }

    toggleHover(event: boolean) {
        this.isHovering = event;
    }


    startUpload(event: FileList) {
        // The File object
        this.downloadURLForEdit = null;
        const file = event.item(0);

        // Client-side validation example
        if (file.type.split('/')[0] !== 'image' && file.type.split('/')[1] !== 'pdf') {
            console.error('unsupported file type :( ');
            return;
        }

        // The storage path
        let path = '';
        if (file.type.split('/')[1] === 'pdf') {
            path = `pdfs/${new Date().getTime()}_${file.name}`;
        } else if (file.type.split('/')[0] === 'image') {
            path = `images/${new Date().getTime()}_${file.name}`;
        }

        // Totally optional metadata
        const customMetadata = {app: 'Nobilis Group App'};

        // The main task
        this.task = this.storage.upload(path, file, {customMetadata});

        // Progress monitoring
        this.percentage = this.task.percentageChanges();
        this.snapshot = this.task.snapshotChanges();

        // The file's download URL
        this.task.snapshotChanges().pipe(
            finalize(() => {
                this.percentage = null;
                this.downloadURL = this.storage.ref(path).getDownloadURL();
                if (this.type.toLowerCase() === 'image') {
                    this.imageUrl.emit(this.downloadURL);
                } else if (this.type.toLowerCase() === 'pdf') {
                    this.pdfUrl.emit((this.downloadURL));
                }
            })
        ).subscribe();


        this.snapshot = this.task.snapshotChanges().pipe(
            tap(snap => {
                if (snap.bytesTransferred === snap.totalBytes) {
                    // Update firestore on completion
                    this.db.collection(`images`).add({path, size: snap.totalBytes});
                }
            })
        );
    }

    setDownloadUrlForForm(path: string): void {
        this.imageService.setImagePath(path);
    }

    // Determines if the upload task is active
    isActive(snapshot) {
        return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
    }

    async deleteItem(url: string) {
        const alert = await this.alertController.create({
            message: `Are you sure you want to delete this file?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: cancel => {
                    },
                },
                {
                    text: 'Delete',
                    cssClass: 'danger',
                    handler: () => {
                        this.downloadURL = null;
                        this.downloadURLForEdit = null;
                        this.storage.storage.refFromURL(url).delete();
                        if (this.type.toLowerCase() === 'image') {
                            this.imageUrl.emit(this.downloadURL);
                            this.fileDeleted.emit({type: 'image'});
                        } else if (this.type.toLowerCase() === 'pdf') {
                            this.pdfUrl.emit(this.downloadURL);
                            this.fileDeleted.emit({type: 'pdf'});
                        }
                    },
                },
            ],
        });

        await alert.present();
    }
}
