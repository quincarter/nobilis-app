import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../core/user';
import {Observable, Subscription} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';
import {LoginPage} from '../../pages/login/login.page';
import {ModalController, NavController} from '@ionic/angular';
import {PageTitle} from '../../models/page-title';
import {Router} from '@angular/router';
import {CreateDashboardLinkPage} from '../../pages/create-dashboard-link/create-dashboard-link.page';
import {CreateContactPage} from '../../pages/create-contact/create-contact.page';
import {CreateReferenceLinkPage} from '../../pages/create-reference-link/create-reference-link.page';
import {PwaService} from '../../services/pwa.service';

@Component({
    selector: 'app-admin-buttons',
    templateUrl: './admin-buttons.component.html',
    styleUrls: ['./admin-buttons.component.scss'],
})
export class AdminButtonsComponent implements OnInit, OnDestroy {

    public user: User;
    public authenticated$: Observable<boolean>;
    public assetDir = '../../../assets/img/';
    public currentUrl: string;
    public createModal: any;

    private authSubscription: Subscription;

    @Input() title: PageTitle;

    constructor(
        public modalController: ModalController,
        public createModalController: ModalController,
        public authService: AuthService,
        public pwa: PwaService,
        private navController: NavController,
        private router: Router,
        private angularFireAuth: AngularFireAuth,
    ) {
    }

    ngOnInit() {
        this.currentUrl = this.router.url;
        this.authService.user$.subscribe(user => this.user = user);
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
    }

    ngOnDestroy(): void {
        this.authSubscription.unsubscribe();
    }

    logout(): void {
        this.authService.signOut();
        if (this.router.url === '/tabs/users') {
            this.navController.navigateRoot('/');
        }
    }


    async presentLoginModal() {
        const modal = await this.modalController.create({
            component: LoginPage
        });
        await modal.present();
    }

    async presentCreateModal() {
        switch (this.title.text) {
            case 'Contact':
                this.createModal = await this.createModalController.create({
                    cssClass: 'hideScrollBar',
                    component: CreateContactPage
                });
                await this.createModal.present();
                break;
            case 'Dashboard':
                this.createModal = await this.createModalController.create({
                    component: CreateDashboardLinkPage
                });
                await this.createModal.present();
                break;
            case 'Reference':
                this.createModal = await this.createModalController.create({
                    component: CreateReferenceLinkPage
                });
                await this.createModal.present();
                break;
            default:
                break;
        }
    }

    private getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }

    installPwa(): void {
        this.pwa.promptEvent.prompt();
    }
}
