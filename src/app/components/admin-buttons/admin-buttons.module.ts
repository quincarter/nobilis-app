import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminButtonsComponent} from './admin-buttons.component';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {LoginPageModule} from '../../pages/login/login.module';
import {CreateContactPageModule} from '../../pages/create-contact/create-contact.module';
import {CreateReferenceLinkPageModule} from '../../pages/create-reference-link/create-reference-link.module';
import {CreateDashboardLinkPageModule} from '../../pages/create-dashboard-link/create-dashboard-link.module';
import {LoginPage} from '../../pages/login/login.page';
import {CreateContactPage} from '../../pages/create-contact/create-contact.page';
import {CreateReferenceLinkPage} from '../../pages/create-reference-link/create-reference-link.page';
import {CreateDashboardLinkPage} from '../../pages/create-dashboard-link/create-dashboard-link.page';

@NgModule({
    declarations: [
        AdminButtonsComponent
    ],
    exports: [
        AdminButtonsComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        CreateReferenceLinkPageModule,
        CreateDashboardLinkPageModule,
        CreateContactPageModule,
        LoginPageModule
    ],
    entryComponents: [
        LoginPage,
        CreateReferenceLinkPage,
        CreateDashboardLinkPage,
        CreateContactPage,
    ]
})
export class AdminButtonsModule {
}
