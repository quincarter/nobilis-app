import {Component, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../../services/auth.service.service';
import {ContactService} from '../../services/contact.service';
import 'rxjs-compat/add/operator/map';
import {ContactInterface} from '../../models/contact.interface';

@Component({
    selector: 'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.scss'],
})
export class ContactListComponent implements OnInit {

    public contacts: Observable<any>;

    private authenticated$: Observable<boolean>;
    private authSubscription: Subscription;

    constructor(
        private angularFireAuth: AngularFireAuth,
        private authService: AuthService,
        private contactService: ContactService,
    ) {
    }

    ngOnInit(): void {
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
        this.getContacts();
    }

    private getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }

    private getContacts(): void {
        this.contacts = this.contactService.getContactList().valueChanges().map((contactData: ContactInterface[]) => {
            return this.sortContacts(contactData, 'lastName', 'asc');
        });
    }

    private sortContacts(data: ContactInterface[], field: string, order: string = 'asc'): ContactInterface[] {
        data.sort((a, b) => {
            switch (field) {
                case 'lastName':
                    return a.lastName.toUpperCase().trim() < b.lastName.toUpperCase().trim() ? -1 : 1;
                case 'firstName':
                    return a.firstName.toUpperCase().trim() < b.firstName.toUpperCase().trim() ? -1 : 1;
                case 'title':
                    return a.title.toUpperCase().trim() < b.title.toUpperCase().trim() ? -1 : 1;
            }
            // sorting contact data by last name ascending
        });
        return data;
    }
}
