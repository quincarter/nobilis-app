import {Component, Input} from '@angular/core';
import {GridListConfig} from '../../models/grid';

@Component({
    selector: 'app-reference-grid',
    templateUrl: './reference-grid.component.html',
    styleUrls: ['./reference-grid.component.scss'],
})
export class ReferenceGridComponent {
    @Input() gridItem: GridListConfig;
}
