import {Component, Input} from '@angular/core';
import {GridListConfig} from '../../models/grid';

@Component({
    selector: 'app-dashboard-grid',
    templateUrl: './dashboard-grid.component.html',
    styleUrls: ['./dashboard-grid.component.scss'],
})
export class DashboardGridComponent {

    @Input() gridItem: GridListConfig;

}
