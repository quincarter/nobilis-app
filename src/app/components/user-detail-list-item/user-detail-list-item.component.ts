import {Component, Input, OnInit} from '@angular/core';
import {IRoles, UserInterface} from '../../models/user.interface';
import {AuthService} from '../../services/auth.service.service';
import {UsersService} from '../../services/users.service';

@Component({
    selector: 'app-user-detail-list-item',
    templateUrl: './user-detail-list-item.component.html',
    styleUrls: ['./user-detail-list-item.component.scss'],
})
export class UserDetailListItemComponent implements OnInit {

    public objectKeys = Object.keys;
    @Input() user: UserInterface;

    constructor(
        public authService: AuthService,
        private userService: UsersService,
    ) {
    }

    ngOnInit() {
    }

    async updateUser(user: UserInterface, role) {
      user.roles[role] = !user.roles[role];
      this.user = {...user};
      return await this.userService.updateUser(user);
    }
}
