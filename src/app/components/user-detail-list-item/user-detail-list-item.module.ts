import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserDetailListItemComponent} from './user-detail-list-item.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
  declarations: [
    UserDetailListItemComponent
  ],
  exports: [
    UserDetailListItemComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class UserDetailListItemModule { }
