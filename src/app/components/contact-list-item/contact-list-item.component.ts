import {Component, Input, OnInit} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import {ContactInterface} from '../../models/contact.interface';
import {ContactDetailPage} from '../../pages/contact-detail/contact-detail.page';

@Component({
    selector: 'app-contact-list-item',
    templateUrl: './contact-list-item.component.html',
    styleUrls: ['./contact-list-item.component.scss'],
})
export class ContactListItemComponent {

    @Input() contact: ContactInterface;

    constructor(
        private modalController: ModalController,
        private navController: NavController,
    ) {
    }

    async goToDetail(contactData: ContactInterface): Promise<void> {
        const modal = await this.modalController.create({
            component: ContactDetailPage,
            componentProps: {contact: {...contactData}},
        });
        await modal.present();
    }
}
