import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', loadChildren: './tabs/tabs.module#TabsPageModule'},
    {path: 'form', loadChildren: './pages/form/form.module#FormPageModule'},
    {path: 'users', loadChildren: './users/users.module#UsersPageModule'},
    {path: 'document', loadChildren: './pages/document/document.module#DocumentPageModule'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
