import {Component, OnInit} from '@angular/core';
import {PageTitle} from '../models/page-title';
import {Observable, Subscription} from 'rxjs';
import {UserInterface} from '../models/user.interface';
import {UsersService} from '../services/users.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from '../services/auth.service.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.page.html',
    styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

    public title: PageTitle;
    public users$: Observable<UserInterface[]>;
    private authSubscription: Subscription;

    constructor(
        public authService: AuthService,
        private angularFireAuth: AngularFireAuth,
        private usersService: UsersService,
    ) {
    }

    ngOnInit(): void {
        this.getAuthSubscription();
        this.setTitle();
        this.getUsers();
    }

    private setTitle() {
        this.title = {
            text: 'Users'
        };
    }

    getUsers(): void {
        this.users$ = this.usersService.getUsersList().valueChanges().map((users: UserInterface[]) => {
            const mainArray = [];
            users.filter((data) => {
                // hiding super admin
                if (data.email !== 'quin.carter@gmail.com' && data.email !== 'quin@quincarter.com') {
                    mainArray.push(data);
                }

            });

            users = mainArray;

            return this.sortUsers(users, 'email');
        });
    }

    private getAuthSubscription(): void {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }

    private sortUsers(data: UserInterface[], field: string, order: string = 'asc'): UserInterface[] {
        data.sort((a, b) => {
            switch (field) {
                case 'email':
                    return a.email.toUpperCase().trim() < b.email.toUpperCase().trim() ? -1 : 1;
            }
            // sorting user data by email ascending
        });
        return data;
    }

}
