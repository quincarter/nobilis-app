import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {UsersPage} from './users.page';
import {AdminButtonsModule} from '../components/admin-buttons/admin-buttons.module';
import {LoginPage} from '../pages/login/login.page';
import {LoginPageModule} from '../pages/login/login.module';
import {AdminGuard} from '../core/admin.guard';
import {UsersListModule} from '../components/users-list/users-list.module';
import {UserDetailPage} from '../pages/user-detail/user-detail.page';
import {UserDetailPageModule} from '../pages/user-detail/user-detail.module';

const routes: Routes = [
    {
        path: '',
        component: UsersPage,
        canActivate: [AdminGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        AdminButtonsModule,
        UsersListModule,
        UserDetailPageModule,
    ],
    entryComponents: [
        UserDetailPage
    ],
    declarations: [UsersPage]
})
export class UsersPageModule {
}
