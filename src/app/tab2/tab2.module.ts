import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab2Page} from './tab2.page';
import {AdminButtonsModule} from '../components/admin-buttons/admin-buttons.module';
import {ReferenceGridComponent} from '../components/reference-grid/reference-grid.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([{path: '', component: Tab2Page}]),
        AdminButtonsModule
    ],
    entryComponents: [],
    declarations: [
        Tab2Page,
        ReferenceGridComponent,
    ]
})
export class Tab2PageModule {
}
