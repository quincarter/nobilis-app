import {Component, OnInit} from '@angular/core';
import {PageTitle} from '../models/page-title';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable, Subscription} from 'rxjs';
import {User} from '../core/user';
import {AuthService} from '../services/auth.service.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AlertController, ModalController} from '@ionic/angular';
import {ContactDetailPage} from '../pages/contact-detail/contact-detail.page';
import {CreateReferenceLinkPage} from '../pages/create-reference-link/create-reference-link.page';
import {ReferenceService} from '../services/reference.service';
import {GridListConfig} from '../models/grid';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

    public user: User;

    public title: PageTitle;
    public grid: Observable<any>;
    public authenticated$: Observable<boolean>;
    private authSubscription: Subscription;

    constructor(
        public authService: AuthService,
        private angularFireAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private alertController: AlertController,
        private modalController: ModalController,
        private referenceService: ReferenceService,
    ) {
    }

    ngOnInit(): void {
        this.setTitle();
        this.authService.user$.subscribe(user => this.user = user);
        this.authenticated$ = this.authService.authenticated$;
        this.getAuthSubscription();
        this.grid = this.afs.collection('reference').valueChanges();
    }

    private setTitle() {
        this.title = {
            text: 'Reference',
            createLink: '/create-reference-link'
        };
    }

    private getAuthSubscription() {
        this.authSubscription = this.angularFireAuth.authState.subscribe(data => {
            if (data) {
                this.authService.setAuthenticated(true);
            } else {
                this.authService.setAuthenticated(false);
            }
        });
    }

    async goToDetail(item: GridListConfig) {
        const modal = await this.modalController.create({
            component: CreateReferenceLinkPage,
            componentProps: {
                editableItem: {...item}
            },
        });
        await modal.present();
    }

    async deleteReference(id, document?, image?) {
        const alert = await this.alertController.create({
            message: `Are you sure you want to delete this reference?`,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: cancel => {
                    },
                },
                {
                    text: 'Delete',
                    cssClass: 'danger',
                    handler: () => {
                        this.referenceService.deleteReference(id, document, image).then(() => {
                            this.modalController.dismiss();
                        });
                    },
                },
            ],
        });

        await alert.present();
    }
}
