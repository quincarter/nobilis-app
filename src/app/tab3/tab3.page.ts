import {Component, OnInit} from '@angular/core';
import {ContactListComponent} from '../components/contact-list/contact-list.component';
import {PageTitle} from '../models/page-title';

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
    public contactList = ContactListComponent;
    title: PageTitle;

    constructor() {
    }

    ngOnInit(): void {
        this.setTitle();
    }

    private setTitle() {
        this.title = {
            text: 'Contact',
            createLink: '/create-contact'
        };
    }
}
