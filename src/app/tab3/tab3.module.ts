import {IonicModule} from '@ionic/angular';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab3Page} from './tab3.page';
import {ContactListComponent} from '../components/contact-list/contact-list.component';
import {ContactListItemComponent} from '../components/contact-list-item/contact-list-item.component';
import {AdminButtonsModule} from '../components/admin-buttons/admin-buttons.module';
import {ContactDetailPage} from '../pages/contact-detail/contact-detail.page';
import {ContactDetailPageModule} from '../pages/contact-detail/contact-detail.module';

const routes: Routes = [
    {
        path: '',
        component: Tab3Page,
        children: [{
            path: ':id',
            component: ContactDetailPage,
        }]
    },
];

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        AdminButtonsModule,
        ContactDetailPageModule,
        FormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        Tab3Page,
        ContactListComponent,
        ContactListItemComponent,
    ],
    entryComponents: [
        ContactListComponent,
        ContactListItemComponent,
        ContactDetailPage,
        Tab3Page,
    ],
})
export class Tab3PageModule {
}
