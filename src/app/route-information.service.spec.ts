import { TestBed } from '@angular/core/testing';

import { RouteInformationService } from './route-information.service';

describe('RouteInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RouteInformationService = TestBed.get(RouteInformationService);
    expect(service).toBeTruthy();
  });
});
